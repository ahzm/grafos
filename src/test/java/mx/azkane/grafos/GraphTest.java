package mx.azkane.grafos;

import mx.azkane.grafos.modelo.CaminoCorto;
import mx.azkane.grafos.modelo.Grafo;
import mx.azkane.grafos.modelo.Vertice;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;

public class GraphTest {
    @Test
    public void calculaCaminoCorto() {
        Vertice nodeA = new Vertice("A");
        Vertice nodeB = new Vertice("B");
        Vertice nodeC = new Vertice("C");
        Vertice nodeD = new Vertice("D");
        Vertice nodeE = new Vertice("E");
        Vertice nodeF = new Vertice("F");

        nodeA.addArista(nodeB, 10);
        nodeA.addArista(nodeC, 15);

        nodeB.addArista(nodeD, 12);
        nodeB.addArista(nodeF, 15);

        nodeC.addArista(nodeE, 10);

        nodeD.addArista(nodeE, 2);
        nodeD.addArista(nodeF, 1);

        nodeF.addArista(nodeE, 5);

        Grafo grafo = new Grafo();

        grafo.addNodo(nodeA);
        grafo.addNodo(nodeB);
        grafo.addNodo(nodeC);
        grafo.addNodo(nodeD);
        grafo.addNodo(nodeE);
        grafo.addNodo(nodeF);

        HashMap<Vertice, CaminoCorto> caminos = Grafo.calculaCaminoCorto(grafo, nodeA);
        assertEquals(caminos.get(nodeA).getDistancia(), 0);
        assertEquals(caminos.get(nodeB).getDistancia(), 10);
        assertEquals(caminos.get(nodeC).getDistancia(), 15);
        assertEquals(caminos.get(nodeD).getDistancia(), 22);
        assertEquals(caminos.get(nodeE).getDistancia(), 24);
        assertEquals(caminos.get(nodeF).getDistancia(), 23);
    }

    @Test
    public void calculaCaminoCorto2() {
        Vertice nodoA = new Vertice("A");
        Vertice nodoB = new Vertice("B");
        Vertice nodoC = new Vertice("C");
        Vertice nodoD = new Vertice("D");

        nodoB.addArista(nodoA, 1);
        nodoC.addArista(nodoB, 2);
        nodoD.addArista(nodoC, 4);

        Grafo grafo = new Grafo();
        grafo.addNodo(nodoA);
        grafo.addNodo(nodoB);
        grafo.addNodo(nodoC);
        grafo.addNodo(nodoD);

        CaminoCorto caminoCorto = Grafo.distanciaMinimaDesdeOrigen(grafo, nodoD, nodoA);

        assertEquals(7, caminoCorto.getDistancia());
    }

    @Test
    public void calculaCaminoCorto3() {
        Vertice nodoA = new Vertice("A");
        Vertice nodoB = new Vertice("B");
        Vertice nodoB1 = new Vertice("B1");
        Vertice nodoC = new Vertice("C");
        Vertice nodoD = new Vertice("D");

        nodoD.addArista(nodoC, 4);
        nodoC.addArista(nodoB, 2);
        nodoC.addArista(nodoB1, 2);

        nodoB.addArista(nodoA, 1);
        nodoB1.addArista(nodoA, 10);

        Grafo grafo = new Grafo();
        grafo.addNodo(nodoA);
        grafo.addNodo(nodoB);
        grafo.addNodo(nodoC);
        grafo.addNodo(nodoD);

        CaminoCorto caminoCorto = Grafo.distanciaMinimaDesdeOrigen(grafo, nodoD, nodoA);

        assertEquals(7, caminoCorto.getDistancia());
    }

    @Test
    public void calculaCaminoCorto4() {
        Vertice nodoA = new Vertice("A");
        Vertice nodoB = new Vertice("B");

        Vertice nodoC = new Vertice("C");
        Vertice nodoD = new Vertice("D");

        nodoD.addArista(nodoC, 4);

        nodoB.addArista(nodoA, 1);

        Grafo grafo = new Grafo();
        grafo.addNodo(nodoA);
        grafo.addNodo(nodoB);
        grafo.addNodo(nodoC);
        grafo.addNodo(nodoD);

        CaminoCorto caminoCorto = Grafo.distanciaMinimaDesdeOrigen(grafo, nodoA, nodoD);

        assertEquals(new CaminoCorto(new ArrayList<>(), 0, nodoA), caminoCorto);

        System.out.println(caminoCorto);
    }

}