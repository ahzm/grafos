package mx.azkane.grafos.controls;

import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Text;

public class Flecha extends Group {

    private final Line    line;
    private final Polygon triangle;
    private final Text    distanceText;
    private       double  startX;
    private       double  startY;
    private       double  endX;
    private       double  endY;
    private       double  spacing;


    Flecha(double startX, double startY, double endX, double endY, double spacing, String distance) {
        super();
        this.startX = startX;
        this.startY = startY;
        this.endX = endX;
        this.endY = endY;
        this.spacing = spacing;

        line = new Line(startX, startY, endX, endY);
        line.setStrokeWidth(4);

        Point2D circlePosition = getCirclePosition(startX, startY, endX, endY, spacing);

        triangle = new Polygon();
        triangle.setFill(Paint.valueOf("RED"));
        triangle.setStrokeWidth(2);
        triangle.setStroke(Paint.valueOf("RED"));
        triangle.setMouseTransparent(true);

        Double[] out = getTrianglePosition(startX, startY, endX, endY, circlePosition);

        triangle.getPoints()
                .addAll(out);
        distanceText = new Text(distance);

        Point2D midpoint = getMidpoint(startX, startY, endX, endY);
        distanceText.setLayoutX(midpoint.getX());
        distanceText.setLayoutY(midpoint.getY());
        distanceText.setStroke(Paint.valueOf("WHITE"));
        distanceText.toFront();

        getChildren().addAll(line, triangle, distanceText);
    }

    private Double[] getTrianglePosition(double startX, double startY, double endX, double endY,
                                         Point2D circlePosition) {
        double angle = Math.atan2(startY - endY, startX - endX);

        double x1 = Math.cos(angle + 0.25) * 30 + circlePosition.getX();
        double y1 = Math.sin(angle + 0.25) * 30 + circlePosition.getY();
        double x2 = Math.cos(angle - 0.25) * 30 + circlePosition.getX();
        double y2 = Math.sin(angle - 0.25) * 30 + circlePosition.getY();

        return new Double[]{circlePosition.getX(), circlePosition.getY(), x1, y1, x2, y2};
    }

    private Point2D getMidpoint(double startX, double startY, double endX, double endY) {
        return new Point2D((startX + endX) / 2, (startY + endY) / 2);
    }

    private Point2D getCirclePosition(double startX, double startY, double endX, double endY,
                                      double spacing) {
        double dist = Math.hypot(startX - endX, startY - endY);
        double r    = spacing / dist;

        double x3 = r * startX + (1 - r) * endX;
        double y3 = r * startY + (1 - r) * endY;

        return new Point2D(x3, y3);
    }

    public void moveEnd(double endX, double endY) {
        this.endX = endX;
        this.endY = endY;

        line.setEndX(endX);
        line.setEndY(endY);

        Point2D circlePosition = getCirclePosition(this.startX,
                                                   this.startY,
                                                   endX,
                                                   endY,
                                                   this.spacing);
        Double[] trianglePosition = getTrianglePosition(this.startX, this.startY, endX, endY, circlePosition);
        triangle.getPoints()
                .setAll(trianglePosition);


        Point2D midpoint = getMidpoint(startX, startY, endX, endY);
        distanceText.setLayoutX(midpoint.getX());
        distanceText.setLayoutY(midpoint.getY());
    }

    public void moveStart(double startX, double startY) {
        this.startX = startX;
        this.startY = startY;

        line.setStartX(startX);
        line.setStartY(startY);

        Point2D circlePosition = getCirclePosition(startX,
                                                   startY,
                                                   this.endX,
                                                   this.endY,
                                                   this.spacing);

        Double[] trianglePosition = getTrianglePosition(startX, startY, this.endX, this.endY, circlePosition);
        triangle.getPoints()
                .setAll(trianglePosition);

        Point2D midpoint = getMidpoint(startX, startY, endX, endY);
        distanceText.setLayoutX(midpoint.getX());
        distanceText.setLayoutY(midpoint.getY());
    }


    public void setStrokeWidth(double value) {line.setStrokeWidth(value);}

    public double getStrokeWidth() {return line.getStrokeWidth();}

    public void setStroke(Paint value) {line.setStroke(value);}

    public Paint getStroke() {return line.getStroke();}
}
