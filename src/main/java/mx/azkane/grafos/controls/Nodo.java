package mx.azkane.grafos.controls;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.rxjavafx.observables.JavaFxObservable;
import io.reactivex.subjects.PublishSubject;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.effect.Bloom;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;
import mx.azkane.grafos.modelo.Vertice;

import java.io.IOException;
import java.util.*;

public class Nodo extends StackPane {
    private static final float nodeRadius = 50f;

    private final MenuItem            itemNodoFuente       = new MenuItem("Nodo origen");
    private final MenuItem            itemNodoDestino      = new MenuItem("Nodo destino");
    private final MenuItem            itemEnlazarNodo      = new MenuItem("Enlazar a este nodo");
    private final MenuItem            itemDeshabilitarNodo = new MenuItem("Deshabilitar");
    private final MenuItem            itemDeleteNodo       = new MenuItem("Borrar nodo");
    private final ContextMenu         nodoCM               = new ContextMenu(itemNodoFuente,
                                                                             itemNodoDestino,
                                                                             itemEnlazarNodo,
                                                                             itemDeshabilitarNodo,
                                                                             itemDeleteNodo);
    private final Map<String, Flecha> linesFromThis        = new HashMap<>();
    private final Map<Flecha, Nodo>   linesToThis          = new HashMap<>();

    private final String  nombre;
    private final Vertice vertice;


    @FXML
    private Text   text;
    @FXML
    private Circle circle;

    private Observable<Nodo> nodoAgregadoObservable;

    private PublishSubject<Nodo>    nodoFuenteObservable     = PublishSubject.create();
    private PublishSubject<Nodo>    nodoDestinoObservable    = PublishSubject.create();
    private PublishSubject<Nodo>    selectedObservable       = PublishSubject.create();
    private PublishSubject<Nodo>    enlazarObservable        = PublishSubject.create();
    private PublishSubject<Nodo>    deleteNodoObserver       = PublishSubject.create();
    private PublishSubject<Boolean> deshabilitarNodoObserver = PublishSubject.create();
    private double     centerX;
    private double     centerY;
    private Disposable selectedSubscriber;
    private Disposable cmSubscriber;
    private Disposable nodoFuenteSubscriber;
    private Disposable nodoDestinoSubscriber;
    private Disposable nodoEnlaceSubscriber;
    private Disposable deleteNodoSubscriber;
    private Disposable deshabilitarNodoSubscriber;


    private Nodo(Vertice vertice) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/mx.azkane.grafos/Nodo.fxml"));
        loader.setRoot(this);
        loader.setController(this);

        try {
            loader.load();
            this.vertice = vertice;
            this.nombre = vertice.getNombre();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Nodo of(Vertice vertice, double x, double y,
                          Observable<Nodo> idNodosAgregados) {
        Nodo nodo = new Nodo(vertice);
        nodo.circle.setFill(Paint.valueOf("BLUE"));
        nodo.circle.setRadius(nodeRadius);

        nodo.text.setBoundsType(TextBoundsType.VISUAL);
        nodo.text.setText(nodo.nombre);
        nodo.text.setFill(Paint.valueOf("WHITE"));
        nodo.text.setMouseTransparent(true);
        nodo.text.setFont(Font.font(null, FontWeight.BOLD, 40));

        nodo.centerX = x - nodeRadius;
        nodo.centerY = y - nodeRadius;
        nodo.setLayoutX(nodo.centerX);
        nodo.setLayoutY(nodo.centerY);

        nodo.nodoAgregadoObservable = idNodosAgregados;

        nodo.setupNodoAgregadoHandler();
        nodo.setupDragging();
        nodo.setupContextMenu();
        nodo.setupSelectable();

        return nodo;
    }

    public Set<Flecha> delete() {
        HashSet<Flecha> relatedFlechas = new HashSet<>(linesFromThis.values());
        relatedFlechas.addAll(linesToThis.keySet());

        for (Map.Entry<Flecha, Nodo> flechaNodoEntry : linesToThis.entrySet()) {
            Nodo otroNodo = flechaNodoEntry.getValue();
            otroNodo.requestDelete(this);
        }

        nodoFuenteObservable.onComplete();
        nodoDestinoObservable.onComplete();
        selectedObservable.onComplete();
        enlazarObservable.onComplete();
        deleteNodoObserver.onComplete();
        deshabilitarNodoObserver.onComplete();
        selectedSubscriber.dispose();
        cmSubscriber.dispose();
        nodoFuenteSubscriber.dispose();
        nodoDestinoSubscriber.dispose();
        nodoEnlaceSubscriber.dispose();
        deleteNodoSubscriber.dispose();
        deshabilitarNodoSubscriber.dispose();

        return relatedFlechas;
    }

    private void requestDelete(Nodo otroNodo) {
        otroNodo.linesFromThis.remove(otroNodo.getNombre());
        vertice.removeArista(otroNodo.getVertice());
    }

    public String getNombre() {
        return nombre;
    }

    private void addLineToThis(Flecha line, Nodo otroNodo) {
        linesToThis.put(line, otroNodo);
    }

    public void setFocus() {
        circle.setStroke(Paint.valueOf("BLACK"));
        circle.setStrokeWidth(3);
    }

    public void unsetFocus() {
        circle.setStroke(null);
    }

    public Optional<Flecha> addNodoAdyacente(Nodo otroNodo, Double dist) {
        if (this.equals(otroNodo)) { return Optional.empty(); }
        vertice.addArista(otroNodo.vertice, dist);
        System.out.println("Linked: " + this.nombre + " " + otroNodo.nombre);

        Flecha flecha = new Flecha(getCenterX(), getCenterY(), otroNodo.getCenterX(), otroNodo.getCenterY(),
                                   nodeRadius, String.valueOf(dist));
        flecha.setMouseTransparent(true);
        flecha.setStroke(Paint.valueOf("BLACK"));
        flecha.setStrokeWidth(5);

        linesFromThis.put(otroNodo.getNombre(), flecha);
        otroNodo.addLineToThis(flecha, this);
        return Optional.of(flecha);
    }

    private double getCenterX() {
        double half = getWidth() / 2;
        return getLayoutX() + half;
    }

    private double getCenterY() {
        double half = getHeight() / 2;
        return getLayoutY() + half;
    }

    private void setupNodoAgregadoHandler() {
        nodoAgregadoObservable.filter(nuevoNodo -> !nuevoNodo.nombre.equals(this.nombre))
                              .filter(nodo -> vertice.esAdyacente(nodo.nombre))
                              .subscribe(nodo -> System.out.println(nombre + " -> " + nodo.nombre));
    }

    private void setupSelectable() {
        // Este nodo al recibir un click, solicita a GraphView estar en foco o seleccionado enviandose
        // por el observable, si el nodo es candidato a enfocarse, GraphView llamara setFocus
        selectedSubscriber = JavaFxObservable.eventsOf(this, MouseEvent.MOUSE_CLICKED)
                                             .filter(me -> me.getButton() == MouseButton.PRIMARY)
                                             .map(me -> this)
                                             .subscribe(selectedObservable::onNext);
    }

    private void setupContextMenu() {
        cmSubscriber = JavaFxObservable.eventsOf(this, MouseEvent.MOUSE_CLICKED)
                                       .subscribe(me -> {
                                           if (me.getButton() == MouseButton.SECONDARY) {
                                               nodoCM.show(this, me.getScreenX(),
                                                           me.getScreenY());
                                           } else {
                                               nodoCM.hide();
                                           }
                                       });

        // GraphView debe mantener una referencia un stream de streams de nodoFuenteObservable, la emision mas
        // reciente sera del nodo fuente, esto puede desencadenar el calculo del camino mas corto si
        // `nodoDestinoObservable' ya emitio
        nodoFuenteSubscriber = JavaFxObservable.actionEventsOf(itemNodoFuente)
                                               .map(ae -> this)
                                               .subscribe(nodoFuenteObservable::onNext);

        // GraphView debe mantener una referencia un stream de streams de `nodoDestinoObservable', la emision mas
        // reciente sera del nodo destino, esto puede desencadenar el calculo del camino mas corto si
        // `nodoFuenteObservable' ya emitio
        nodoDestinoSubscriber = JavaFxObservable.actionEventsOf(itemNodoDestino)
                                                .map(ae -> this)
                                                .subscribe(nodoDestinoObservable::onNext);

        // GraphView debe tener una referencia al observable de selected, al recibir una peticion de enlace
        // el nodo seleccionado agregara un nuevo arista con el vertice representado por este nodo
        // Se dibujara un Path del nodo seleccionado a este nodo
        nodoEnlaceSubscriber = JavaFxObservable.actionEventsOf(itemEnlazarNodo)
                                               .map(ae -> this)
                                               .subscribe(enlazarObservable::onNext);

        // Al desabilitar un nodo, este se marca como deshabilitado en el modelo, esto puede desencadenar el calculo
        // del camino mas corto.
        deshabilitarNodoSubscriber = JavaFxObservable.actionEventsOf(itemDeshabilitarNodo)
                                                     .map(ae -> this)
                                                     .subscribe(Nodo::toggleHabilitadoStatus);

        // Remueve el nodo del modelo, desencadena redibujar los caminos y calcular el camino mas corto
        deleteNodoSubscriber = JavaFxObservable.actionEventsOf(itemDeleteNodo)
                                               .map(ae -> this)
                                               .subscribe(deleteNodoObserver::onNext);
    }

    private void setupDragging() {
        JavaFxObservable.eventsOf(this, MouseEvent.MOUSE_ENTERED)
                        .subscribe(me -> {
                            text.setEffect(new Bloom(0.1));
                        });
        JavaFxObservable.eventsOf(circle, MouseEvent.MOUSE_EXITED)
                        .subscribe(me -> text.setEffect(null));

        JavaFxObservable.eventsOf(this, MouseEvent.MOUSE_DRAGGED)
                        .filter(me -> {
                            double parentWidth = getParent()
                                    .getLayoutBounds()
                                    .getWidth();

                            double parentHeight = getParent()
                                    .getLayoutBounds()
                                    .getHeight();
                            return me.getSceneX() > 0 &&
                                    me.getSceneY() > 0 &&
                                    me.getSceneX() < parentWidth - getWidth() &&
                                    me.getSceneY() < parentHeight - getHeight();
                        })
                        .subscribe(me -> {
                            relocate(me.getSceneX() - (this.getWidth() / 2),
                                     me.getSceneY() - (this.getHeight() / 2));
                            linesToThis.keySet()
                                       .forEach(flecha -> {
                                           flecha.moveEnd(this.getCenterX(), this.getCenterY());
                                       });

                            linesFromThis.values()
                                         .forEach(flecha -> {
                                             flecha.moveStart(this.getCenterX(), this.getCenterY());
                                         });
                        });
    }

    public Flecha maybeDrawPath(List<Vertice> vertices) {
        int idx = vertices.indexOf(this.vertice);

        if (idx == -1 || idx == vertices.size() - 1) { return null; }

        String nombre = vertices.get(idx + 1)
                                .getNombre();
        Flecha flecha = linesFromThis.get(nombre);

        flecha.setStroke(Paint.valueOf("GREEN"));

        return flecha;
    }

    //TODO: toggleHabilitadoStatus debe tomar una bandera que indica si debe pasar el evento
    // a deshabilitarNodoObserver, esto para cuando desactiva nodos de manera automatica esta
    // activo
    private void toggleHabilitadoStatus() {
        boolean habilitado  = !vertice.isHabilitado();
        Paint   paint       = habilitado ? Paint.valueOf("BLACK") : Paint.valueOf("GRAY");
        Paint   circlePaint = habilitado ? Paint.valueOf("BLUE") : Paint.valueOf("GRAY");
        String  label       = habilitado ? "Habilitar nodo" : "Deshabilitar nodo";

        linesFromThis.values()
                     .forEach(f -> f.setStroke(paint));
        linesToThis.keySet()
                   .forEach(f -> f.setStroke(paint));

        circle.setFill(circlePaint);
        itemDeshabilitarNodo.setText(label);

        vertice.setHabilitado(habilitado);

        deshabilitarNodoObserver.onNext(habilitado);
    }

    public void setHabilitadoStatus() {
        vertice.setHabilitado(true);
        circle.setFill(Paint.valueOf("BLUE"));
        linesToThis.keySet()
                   .forEach(f -> f.setStroke(Paint.valueOf("BLACK")));
        linesFromThis.values()
                     .forEach(f -> f.setStroke(Paint.valueOf("BLACK")));
        itemDeshabilitarNodo.setText("Deshabilitar nodo");
    }

    public void setDeshabilitado() {
        vertice.setHabilitado(false);
        circle.setFill(Paint.valueOf("GRAY"));
        linesToThis.keySet()
                   .forEach(f -> f.setStroke(Paint.valueOf("GRAY")));
        linesFromThis.values()
                     .forEach(f -> f.setStroke(Paint.valueOf("GRAY")));
        itemDeshabilitarNodo.setText("Habilitar nodo");
    }

    public Observable<Nodo> getSelectedObservable() {
        return selectedObservable.share();
    }

    public Observable<Nodo> getEnlazarObservable() {
        return enlazarObservable.share();
    }

    public Observable<Nodo> getNodoFuenteObservable() {
        return nodoFuenteObservable.share();
    }

    public Observable<Nodo> getNodoDestinoObservable() {
        return nodoDestinoObservable.share();
    }

    public Observable<Nodo> getDeleteNodoObserver() {
        return deleteNodoObserver.share();
    }

    public Observable<Boolean> getDeshabilitarNodoObserver() {
        return deshabilitarNodoObserver.share();
    }

    public Vertice getVertice() {
        return vertice;
    }
}
