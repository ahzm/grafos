package mx.azkane.grafos.modelo;

import java.util.List;
import java.util.Objects;

public class CaminoCorto {
    private List<Vertice> caminoCorto;
    private double        distancia;
    private Vertice       verticeDestino;

    public CaminoCorto(List<Vertice> caminoCorto, int distancia, Vertice verticeDestino) {
        this.caminoCorto = caminoCorto;
        this.distancia = distancia;
        this.verticeDestino = verticeDestino;
    }

    public void setCaminoCorto(List<Vertice> caminoCorto) {
        this.caminoCorto = caminoCorto;
    }

    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }

    public List<Vertice> getCaminoCorto() {
        return caminoCorto;
    }

    public double getDistancia() {
        return distancia;
    }

    public Vertice getVerticeDestino() {
        return verticeDestino;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        CaminoCorto that = (CaminoCorto) o;
        return distancia == that.distancia &&
                Objects.equals(caminoCorto, that.caminoCorto) &&
                Objects.equals(verticeDestino, that.verticeDestino);
    }

    @Override
    public int hashCode() {

        return Objects.hash(caminoCorto, distancia, verticeDestino);
    }

    @Override
    public String toString() {
        return "CaminoCorto{" +
                "caminoCorto=" + caminoCorto +
                ", distancia=" + distancia +
                '}';
    }
}
