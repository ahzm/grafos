package mx.azkane.grafos.modelo;

public interface Posicionable {
    double getX();

    double getY();
}
