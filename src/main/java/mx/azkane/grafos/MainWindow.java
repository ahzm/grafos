package mx.azkane.grafos;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.rxjavafx.observables.JavaFxObservable;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import mx.azkane.grafos.modelo.Vertice;
import mx.azkane.grafos.serialization.GrafoMarshaller;
import mx.azkane.grafos.serialization.GrafoSerializable;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MainWindow {
    @FXML
    private CheckBox         desactivaVertices;
    @FXML
    private MenuItem         abrir;
    @FXML
    private MenuItem         guardar;
    @FXML
    private MenuItem         reiniciar;
    @FXML
    private MenuItem         configuracion;
    @FXML
    private MenuItem         cerrar;
    @FXML
    private Label            sourceNodeLabel;
    @FXML
    private Label            destNodeLabel;
    @FXML
    private Label            distBetweenNodes;
    @FXML
    private BorderPane       baseWindowBorderPane;
    @FXML
    private ListView<String> listaRuta;

    private GraphView graphView;
    private FileChooser fileChooser = new FileChooser();
    private Disposable nodoFuenteDisposer;
    private Disposable nodoDestinoDisposer;
    private Disposable idstanciaShortestDisposer;
    private Disposable lastShortestPath;

    @FXML
    private void initialize() {
        sourceNodeLabel.setText("");
        destNodeLabel.setText("");
        distBetweenNodes.setText("");

        fileChooser.setTitle("Abrir grafo");
        fileChooser.getExtensionFilters()
                   .add(new FileChooser.ExtensionFilter("Archivos de grafos (*.grf)",
                                                        "*.grf"));

        setupAbrirAction();
        setupGuardarAction();

        loadGraphView();

        setupReiniciar();

        setupCerrar();

        inicializaGraphViewObservers();
    }

    private void setupCerrar() {
        JavaFxObservable.actionEventsOf(cerrar)
                        .subscribe(ae -> {
                            Platform.exit();
                            System.exit(0);
                        });
    }

    private void inicializaGraphViewObservers() {
        nodoFuenteDisposer = this.graphView.getNodoFuente()
                                           .subscribe(nodoId -> sourceNodeLabel.setText(nodoId.getNombre()));

        nodoDestinoDisposer = this.graphView.getNodoDestino()
                                            .subscribe(nodoId -> destNodeLabel.setText(nodoId.getNombre()));

        idstanciaShortestDisposer =
                this.graphView.getDistanciaShortestPath()
                              .subscribe(dist -> distBetweenNodes.setText(String.format("%.02f", dist)));

        lastShortestPath = this.graphView.getUltimoShortestPath()
                                         .map(caminoCorto -> {
                                             ArrayList<String> ret            = new ArrayList<>();
                                             List<Vertice>     caminoVisitado = caminoCorto.getCaminoCorto();
                                             for (int i = 0; i < caminoVisitado.size() - 1; i++) {
                                                 Vertice v1 = caminoVisitado.get(i);
                                                 Vertice v2 = caminoVisitado.get(i + 1);
                                                 ret.add(String.format("%s -> %s :: %.02f", v1.getNombre(),
                                                                       v2.getNombre(),
                                                                       v1.getDistanciaToAdyacente(v2)));
                                             }
//                                             if (caminoVisitado.size() > 0) {
//                                                 Vertice penultimoVertice = caminoVisitado.get(
//                                                         caminoVisitado.size() - 1);
//                                                 ret.add(String.format("%s -> %s :: %.02f",
//                                                                       penultimoVertice.getNombre(),
//                                                                       caminoCorto.getVerticeDestino()
//                                                                                  .getNombre(),
//                                                                       penultimoVertice.getDistanciaToAdyacente(
//                                                                               caminoCorto.getVerticeDestino())));
//                                             }

                                             return ret;
                                         })
                                         .subscribe(caminoVisitado -> {
                                             listaRuta.getItems()
                                                      .clear();
                                             listaRuta.getItems()
                                                      .addAll(caminoVisitado);
                                         });

        this.graphView.setDesactivaVerticesObserver(JavaFxObservable.valuesOf(desactivaVertices.selectedProperty()));
    }

    private void setupReiniciar() {
        JavaFxObservable.actionEventsOf(reiniciar)
                        .subscribe(ae -> reiniciarGraphView());
    }

    private void reiniciarGraphView() {
        nodoFuenteDisposer.dispose();
        nodoDestinoDisposer.dispose();
        idstanciaShortestDisposer.dispose();
        lastShortestPath.dispose();

        baseWindowBorderPane.setCenter(null);

        loadGraphView();

        inicializaGraphViewObservers();
    }

    private GraphView getGraphView() {
        return graphView;
    }

    private void setupGuardarAction() {
        JavaFxObservable.actionEventsOf(guardar)
                        .flatMap(ae -> Observable.fromCallable(
                                () -> {
                                    fileChooser.setTitle("Guardar grafo");
                                    return Optional.ofNullable(
                                            fileChooser.showSaveDialog(baseWindowBorderPane.getScene()
                                                                                           .getWindow()));
                                }))
                        .subscribe(fileOpt -> {
                            fileOpt.map(f -> {
                                if (!f.getName()
                                      .endsWith(".grf")) {
                                    f = new File(f.getAbsolutePath() + ".grf");
                                }

                                GrafoSerializable grafoSerializable = getGraphView().dumpState();
                                GrafoMarshaller.marshallGrafo(f, grafoSerializable);

                                return f.exists();
                            });
                        });
    }

    private void setupAbrirAction() {
        JavaFxObservable.actionEventsOf(abrir)
                        .flatMap(ae -> Observable.fromCallable(
                                () -> {
                                    fileChooser.setTitle("Abrir grafo");
                                    return Optional.ofNullable(
                                            fileChooser.showOpenDialog(baseWindowBorderPane.getScene()
                                                                                           .getWindow()));
                                }))
                        .subscribe(f -> {
                            f.map(file -> {
                                GrafoSerializable grafoSerializable = GrafoMarshaller.unmarshallGrafo(file);
                                System.out.println(grafoSerializable);
                                // TODO: Handle null from GrafoMarshaller
                                if (grafoSerializable == null) {
                                    System.out.println("No se pudo cargar grafo");
                                }

                                reiniciarGraphView();

                                return grafoSerializable;
                            })
                             .map(getGraphView()::loadState)
                             .ifPresent(res -> System.out.println("Se cargo correctamente"));
                        });
    }

    private void loadGraphView() {
        FXMLLoader graphLoader = new FXMLLoader(getClass().getResource("/mx.azkane.grafos/GraphView.fxml"));
        try {
            Parent graphViewRoot = graphLoader.load();
            this.graphView = graphLoader.getController();
            baseWindowBorderPane.setCenter(graphViewRoot);
        } catch (IOException e) {
            throw new IllegalStateException("Error al cargar GraphView.fxml: " + e.getMessage());
        }
    }
}
