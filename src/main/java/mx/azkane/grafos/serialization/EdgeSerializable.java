package mx.azkane.grafos.serialization;

import mx.azkane.grafos.controls.Nodo;
import mx.azkane.grafos.modelo.Vertice;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@XmlRootElement(namespace = "mx.azkane.grafos.serialization.GrafoSerializable")
public class EdgeSerializable {
    private String origen;
    private String destino;
    private Double distance;

    public static Set<EdgeSerializable> of(Nodo nodo) {
        Map<Vertice, Double> nodosAdyacentes = nodo.getVertice()
                                                   .getNodosAdyacentes();
        return nodosAdyacentes.entrySet()
                              .stream()
                              .map(verticeDist -> {
                                  Vertice vertice = verticeDist.getKey();
                                  Double  dist    = verticeDist.getValue();
                                  return new EdgeSerializable().setOrigen(nodo.getVertice()
                                                                              .getNombre())
                                                               .setDestino(vertice.getNombre())
                                                               .setDistance(dist);
                              })
                              .collect(Collectors.toSet());
    }

    public String getDestino() {
        return destino;
    }

    public EdgeSerializable setDestino(String destino) {
        this.destino = destino;
        return this;
    }

    public Double getDistance() {
        return distance;
    }

    public EdgeSerializable setDistance(Double distance) {
        this.distance = distance;
        return this;
    }

    public String getOrigen() {
        return origen;
    }

    public EdgeSerializable setOrigen(String origen) {
        this.origen = origen;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        EdgeSerializable that = (EdgeSerializable) o;
        return Objects.equals(origen, that.origen) &&
                Objects.equals(destino, that.destino);
    }

    @Override
    public int hashCode() {

        return Objects.hash(origen, destino);
    }

    @Override
    public String toString() {
        return "EdgeSerializable{" +
                "origen='" + origen + '\'' +
                ", destino='" + destino + '\'' +
                ", distance=" + distance +
                '}';
    }
}
