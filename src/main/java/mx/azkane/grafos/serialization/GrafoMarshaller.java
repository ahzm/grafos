package mx.azkane.grafos.serialization;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class GrafoMarshaller {
    public static void marshallGrafo(File grafoFile, GrafoSerializable grafo) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(GrafoSerializable.class);
            Marshaller  marshaller  = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(grafo, System.out);
            marshaller.marshal(grafo, grafoFile);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public static GrafoSerializable unmarshallGrafo(File grafoFile) {
        try {
            JAXBContext       jaxbContext  = JAXBContext.newInstance(GrafoSerializable.class);
            Unmarshaller      unmarshaller = jaxbContext.createUnmarshaller();
            GrafoSerializable grafo        = (GrafoSerializable) unmarshaller.unmarshal(grafoFile);
            System.out.println(grafo);
            return grafo;
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return null;
    }
}
